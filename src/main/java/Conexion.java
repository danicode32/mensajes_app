import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {
    private static Connection instance = null;

    private Conexion() { }

    public static Connection getConnection() {
        if (instance == null) {
            try {
                //3306 es el puerto por defecto al momento de instalar xampp.
                final String URL = "jdbc:mysql://localhost:3306/mensajes_app";
                //Igualmente root es el usuario por defecto.
                final String USER = "root";
                //De igual manera, la contraseña es una cadena vacía.
                final String PASSWORD = "";

                instance = DriverManager.getConnection(URL, USER, PASSWORD);
                System.out.println();

            } catch (SQLException ex) {
                System.out.println("Error " + ex.getErrorCode() + ": " + ex);
            }
        }

        return instance;
    }
}

class Conexion2 {
    private static Connection myConnection;

    public static Connection getConnection(){
        if(myConnection == null) {
            try {
                myConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mensajes_app", "root", "op277353");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return myConnection;
    }
}
