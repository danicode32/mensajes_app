public class Message {
    private int messageId;
    private String message, author, date;


    public Message() { }

    public Message(int id, String message) {
        this.messageId = id;
        this.message = message;
    }

    public Message(String message, String messageAuthor) {
        this.message = message;
        this.author = messageAuthor;
    }

    public Message(String message, String messageAuthor, String messageDate) {
        this.message = message;
        this.author = messageAuthor;
        this.date = messageDate;
    }

    public Message(int messageId, String message, String author, String date) {
        this.messageId = messageId;
        this.message = message;
        this.author = author;
        this.date = date;
    }

    public void setMessageId(int id) { this.messageId = id; }

    public void setMessage(String message) { this.message = message; }

    public void setAuthor(String author) { this.author = author; }

    public void setDate(String date) { this.date = date; }

    public int getMessageId() { return this.messageId; }

    public String getMessage() { return this.message; }

    public String getAuthor() { return this.author; }

    public String getDate() { return this.date; }

    @Override
    public String toString() {
        return "Message{" +
                "messageId=" + messageId +
                ", message='" + message + '\'' +
                ", messageAuthor='" + author + '\'' +
                ", messageDate='" + date + '\'' +
                '}';
    }
}
