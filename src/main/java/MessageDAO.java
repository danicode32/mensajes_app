import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MessageDAO {
    private static MessageDAO instance = null;

    private MessageDAO() { }

    public static MessageDAO getDAO() {
        return instance != null ? instance : new MessageDAO();
    }


    public void create(Message message) {
        try (Connection connection = Conexion.getConnection()) {
            PreparedStatement preparedStatement = null;

            try {
                String query = "INSERT INTO mensajes (mensaje, autor_mensaje) VALUES (?, ?)";

                preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, message.getMessage());
                preparedStatement.setString(2, message.getAuthor());
                preparedStatement.executeUpdate();
                System.out.println("\nEl mensaje fue creado correctamente.\n");
            } catch (SQLException ex) {
                System.out.println("Error " + ex.getErrorCode() + ": " + ex);
            }

        } catch (SQLException ex) {
            System.out.println("Error " + ex.getErrorCode() + ": " + ex);
        }
    }

    public List<Message> read() {
        List<Message> messages = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet resultSet = null;

        try (Connection connection = Conexion.getConnection()) {
            String query = "SELECT * FROM mensajes";

            ps = connection.prepareStatement(query);
            resultSet = ps.executeQuery();

            while (resultSet.next()) {
                messages.add(
                        new Message(
                        resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4)
                ));
            }

        } catch (SQLException ex) {
            System.out.println("Error " + ex.getErrorCode() + ": " + ex);
        }

        return messages;
    }

    public void update(Message message) {
        try (Connection connection = Conexion.getConnection()) {
            PreparedStatement ps = null;

            try {
                String query = "UPDATE mensajes SET mensaje = ? WHERE id_mensaje = ?";
                ps = connection.prepareStatement(query);
                ps.setString(1, message.getMessage());
                ps.setInt(2, message.getMessageId());
                ps.executeUpdate();
                System.out.println("El mensaje ha sido actualizado");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void delete(int id) {
        try (Connection connection = Conexion.getConnection()) {
            PreparedStatement ps = null;

            try {
                String query = "DELETE FROM mensajes WHERE id_mensaje = ?";
                ps = connection.prepareStatement(query);
                ps.setInt(1, id);

                int rowsUpdated = ps.executeUpdate();

                System.out.println(rowsUpdated != 0 ? "Mensaje borrado correctamente" : "ID no encontrado");

            } catch (SQLException e) {
                e.printStackTrace();
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
