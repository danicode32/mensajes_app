import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MessageService {
    private static MessageService instance = null;
    private static MessageDAO dao = MessageDAO.getDAO();
    private final Scanner sc = new Scanner(System.in);

    private MessageService() { }

    public static MessageService getMessages() {
        return instance != null ? instance : new MessageService();
    }

    public void createMessage() {
        System.out.print("Escribe un mensaje: ");
        String message = sc.nextLine();

        System.out.print("Autor: ");
        String author = sc.nextLine();

        Message loggedMessage = new Message(message, author);
        dao.create(loggedMessage);
    }

    public void listMessages() {
        List<Message> messages = dao.read();

        messages.forEach(message -> System.out.println(
                "--------- ID: " + message.getMessageId() + " ---------\n" +
                        "Autor: " + message.getAuthor() + "\n" +
                        "Mensaje: " + message.getMessage() + "\n" +
                        "Fecha: " + message.getDate() + "\n\n"
        ));
    }

    public void updateMessage() {
        System.out.print("Escribe tu nuevo mensaje: ");
        String message = sc.nextLine();

        System.out.print("ID del mensaje: ");
        int id = Integer.parseInt(sc.nextLine());

        Message updated = new Message(id, message);
        dao.update(updated);
    }

    public void deleteMessage() {
        System.out.print("ID del mensaje a borrar: ");
        int id = sc.nextInt();
        dao.delete(id);
    }
}
