import java.sql.Connection;
import java.util.Scanner;

public class Start {
    private static final Scanner sc = new Scanner(System.in);

    public static void main(String... danicode32) {
        runApp();
        sc.close();
    }

    private static void showMenu() {
        System.out.println("-------------------------");
        System.out.println("¡Bienvenido!\nElige una de las opciones:\n\n" +
                "1. Crear mensaje.\n" +
                "2. Listar mensajes.\n" +
                "3. Actualizar mensaje.\n" +
                "4. Borrar mensaje.\n" +
                "5. Salir.\n");

        System.out.print("Opción seleccionada: ");
    }

    private static void runApp() {
        int option;

        do {
            showMenu();
            option = sc.nextInt();

            logic(option);
        } while (option != 5);
    }

    private static void logic(int option) {
        final MessageService messages = MessageService.getMessages();

        switch (option) {
            case 1:
                messages.createMessage();
                break;
            case 2:
                messages.listMessages();
                break;
            case 3:
                messages.updateMessage();
                break;
            case 4:
                messages.deleteMessage();
                break;
            case 5:
                System.out.println("\nHasta pronto!");
                System.exit(0);
        }
    }
}
